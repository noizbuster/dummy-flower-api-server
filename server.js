var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

var port = process.env.PORT || 5555;

var router = express.Router();

router.get('/', function(req, res) {
    res.json({message: 'hooray! welcome to our api!'});
});

//for local debug
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

router.route('/tasks')
.get(function(req, res) {
    console.log("someone accessed dummy");
	min = 1000 * 60;
	hour = min * 60;
    now = new Date().getTime();
    task1s = new Date(now - (6 * hour));	//success
    task1e = new Date(task1s.getTime() + (30 * min));

    task2s = new Date(now - (5 * hour));	//revoked
    task2e = new Date(task2s.getTime() + (30 * min));

    task3s = new Date(now - (4 * hour));	//success
    task3e = new Date(task3s.getTime() + (30 * min));

    task4s = new Date(now - (3 * hour));	//failure
    task4e = new Date(task4s.getTime() + (30 * min));

    task5s = new Date(now - (2 * hour));	//success
    task5e = new Date(task5s.getTime() + (30 * min));

    task6s = new Date(now - (30 * min));	//STARTED
    task6e = null;

    task7s = null;				//PENDING
    task7e = null;

    task8s = new Date(now + (2 * hour));	//PENDING
    task8e = null;

    msg = {
        "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c1": {
            "args": "[1, 1]",
            "client": null,
            "clock": 1079,
            "eta": null,
            "exception": null,
            "exchange": null,
            "expires": null,
            "failed": null,
            "kwargs": "{}",
            "name": "tasks.add",
            "received": 1398505411.107885,
            "result": "'2'",
            "retried": null,
            "retries": 0,
            "revoked": null,
            "routing_key": null,
            "runtime": (task1e.getTime() - task1s.getTime())/1000,
            "sent": null,
            "started": task1s.getTime()/1000,
            "state": "SUCCESS",
            "succeeded": task1e.getTime()/1000,
            "timestamp": 1398505411.124802,
            "traceback": null,
            "uuid": "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c1"
        },
        "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c2": {
            "args": "[3, 4]",
            "client": null,
            "clock": 1079,
            "eta": null,
            "exception": null,
            "exchange": null,
            "expires": null,
            "failed": null,
            "kwargs": "{}",
            "name": "tasks.add",
            "received": 1398505411.107885,
            "result": "'7'",
            "retried": null,
            "retries": 0,
            "revoked": task2e.getTime()/1000,
            "routing_key": null,
            "runtime": task2e.getTime()/1000 - task2s.getTime()/1000,
            "sent": null,
            "started": task2s.getTime()/1000,
            "state": "REVOKED",
            "succeeded": null,
            "timestamp": 1398505411.124802,
            "traceback": null,
            "uuid": "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c2"
        },
        "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c3": {
            "args": "[3, 4]",
            "client": null,
            "clock": 1079,
            "eta": null,
            "exception": null,
            "exchange": null,
            "expires": null,
            "failed": null,
            "kwargs": "{}",
            "name": "tasks.add",
            "received": 1398505411.107885,
            "result": "'7'",
            "retried": null,
            "retries": 0,
            "revoked": null,
            "routing_key": null,
            "runtime": task3e.getTime()/1000 - task3s.getTime()/1000,
            "sent": null,
            "started": task3s.getTime()/1000,
            "state": "SUCCESS",
            "succeeded": task3e.getTime()/1000,
            "timestamp": 1398505411.124802,
            "traceback": null,
            "uuid": "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c3"
        },
        "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c4": {
            "args": "[3, 4]",
            "client": null,
            "clock": 1079,
            "eta": null,
            "exception": null,
            "exchange": null,
            "expires": null,
            "failed": task4e.getTime()/1000,
            "kwargs": "{}",
            "name": "tasks.add",
            "received": 1398505411.107885,
            "result": "'7'",
            "retried": null,
            "retries": 0,
            "revoked": null,
            "routing_key": null,
            "runtime": task4e.getTime()/1000 - task4s.getTime()/1000,
            "sent": null,
            "started": task4s.getTime()/1000,
            "state": "FAILURE",
            "succeeded": null,
            "timestamp": 1398505411.124802,
            "traceback": null,
            "uuid": "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c4"
        },
        "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c5": {
            "args": "[3, 4]",
            "client": null,
            "clock": 1079,
            "eta": null,
            "exception": null,
            "exchange": null,
            "expires": null,
            "failed": null,
            "kwargs": "{}",
            "name": "tasks.add",
            "received": 1398505411.107885,
            "result": "'7'",
            "retried": null,
            "retries": 0,
            "revoked": null,
            "routing_key": null,
            "runtime": task5e.getTime()/1000 - task5s.getTime()/1000,
            "sent": null,
            "started": task5s.getTime()/1000,
            "state": "SUCCESS",
            "succeeded": task5e.getTime()/1000,
            "timestamp": 1398505411.124802,
            "traceback": null,
            "uuid": "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c5"
        },
        "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c6": {
            "args": "[1, 2]",
            "client": null,
            "clock": 1042,
            "eta": null,
            "exception": null,
            "exchange": null,
            "expires": null,
            "failed": null,
            "kwargs": "{}",
            "name": "tasks.add",
            "received": 1398505395.327208,
            "result": "'3'",
            "retried": null,
            "retries": 0,
            "revoked": null,
            "routing_key": null,
            "runtime": null,
            "sent": null,
            "started": task6s.getTime()/1000,
            "state": "STARTED",
            "succeeded": null,
            "timestamp": 1398505395.341089,
            "traceback": null,
            "uuid": "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c6"
        },
        "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c7": {
            "args": "[1, 2]",
            "client": null,
            "clock": 1042,
            "eta": null,
            "exception": null,
            "exchange": null,
            "expires": null,
            "failed": null,
            "kwargs": "{}",
            "name": "tasks.add",
            "received": 1398505395.327208,
            "result": "'3'",
            "retried": null,
            "retries": 0,
            "revoked": null,
            "routing_key": null,
            "runtime": null,
            "sent": null,
            "started": null,
            "state": "PENDING",
            "succeeded": null,
            "timestamp": 1398505395.341089,
            "traceback": null,
            "uuid": "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c7"
        },
        "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c8": {
            "args": "[1, 2]",
            "client": null,
            "clock": 1042,
            "eta": null,
            "exception": null,
            "exchange": null,
            "expires": null,
            "failed": null,
            "kwargs": "{}",
            "name": "tasks.add",
            "received": 1398505395.327208,
            "result": "'3'",
            "retried": null,
            "retries": 0,
            "revoked": null,
            "routing_key": null,
            "runtime": null,
            "sent": null,
            "started": null,
            "state": "PENDING",
            "succeeded": null,
            "timestamp": 1398505395.341089,
            "traceback": null,
            "uuid": "e42ceb2d-8730-47b5-8b4d-8e0d2a1ef7c8"
        }
    };
    res.json(msg);
})
.put(function(req, res) {
    //do nothing
});

app.use('/api', router);

app.listen(port);
console.log('dummy server on port ' + port);
